function getExampleComLinks () {
  $.ajax({
    method: 'POST',
    url:"http://localhost:9000/api/links",
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    data: JSON.stringify({url: 'http://example.com'}),
  }).done(function(response) {
    //Using separate div and changing DOM used for simplicity, better to use events/sockets/etc
    $('#testApiLinks').html(JSON.stringify(response))
  });
}
