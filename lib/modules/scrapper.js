//Abstract method can be reused
var scrape = function scrape(url, selector, method, params, cb) {
  //Using request to avoid code duplicity and reingeneering internals
  var request = require('request');
  //Using cheerio as simple and fast jquery implementation
  //This can be done with native methods but this method wouldn't be so reusable
  var cheerio = require('cheerio');

  request(url, function(error, response, body){
    if (error) throw new Error(error);
    var $ = cheerio.load(body);
    
    var elements = [];
    $(selector).each(function(){
      elements.push($(this)[method].apply($(this), params));
    });

    cb({links: elements});
  })
}

module.exports = {
  //While interface is easy to understand, just url and callback
  getLinksFromUrl: function getLinksFromUrl (url, cb) {
    scrape(url, 'a', 'attr', ['href'], cb);
  }
}
