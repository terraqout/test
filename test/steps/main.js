var assert = require('assert');
var English = require('yadda').localisation.English;
var request = require('request');
var jsdom = require('jsdom');
var fs = require('fs');

//Global variables used for simplictiry, better to implement "scope" object and clear on teardown
var server;
var endpoint;
var jsonResponse;
var jsdomWindow;

//steps names I think is declaring what they do clearly, so commments would duplicate explanations

module.exports = (function() {
  return English.library()
  .given("simple node.js server", function(next){
      server = require('../../server');
      server.run(next);
  })
  .then("it listens on $URL", function(url, next){
      request(url, function(error,response, body){
        if (error) throw new Error(error);
        next();
      })
  })
  .then("$URL response $RESPONSE", function(url, testResponse, next){
      request(url, function(error, response, body) {
        if (error) throw new Error(error);
        assert.equal(body, testResponse);
        next();
      });
  })
  .given("POST endpoint: $URL", function(url,next){
    request.post(url, function(error,response, body){
        if (response.body !== '"Specify url"')
          throw new Error(JSON.stringify(response));
        endpoint = url;
        next()
    });
  })
  .given("POST body $BODY", function(body,next){
    request.post({url: endpoint, body: JSON.parse(body), json: true}, function(error, response, body) {
      jsonResponse = body;
      next();
    });
  })
  .then("JSON response $RESPONSE", function(testReponse,next){
    assert.equal(JSON.parse(testReponse).url, jsonResponse.url);
    next()
  })
  .given("open $URL", function(url, next){
    jsdom.env({
      url: url,
      features: {
        FetchExternalResources: ["script"],
        ProcessExternalResources: ["script"],
        SkipExternalResources: false
      },
      done: function(error, window){
        if (error) throw new Error(error);
        jsdomWindow = window;
        next();
      }
    });
  })
  .then("$id contents equal $CONTENT", function(id, testContent, next){
    //Recursive content waiter
    (function getContentById() {
      var jsdomContent = jsdomWindow.$('#'+id).text();
      if (!jsdomContent) return setInterval(getContentById, 500);
      assert.deepEqual(JSON.parse(jsdomContent), JSON.parse(testContent));
      next();
      next=function(){}; //Fixing multiple calls of callback
     })();
  })
  .then("contents of $FILE should be $CONTENT", function(file, testContent, next){
    fs.readFile(file, function(e,content){
      if (e) throw new Error(e);
      assert.deepEqual(JSON.parse(content), JSON.parse(testContent));
      next();
    })
  })
  .then("terminate server", function(next){
    server.terminate(next)
  })
  .then("remove links files", function(next){
    fs.readdir("links", function(error, files){
      if (error) throw new Error(error);
      for (var i in files) {
        if (files[i] != '.gitkeep')
          fs.unlinkSync('links/'+files[i])
      }
      next();
    })
  })
})();
