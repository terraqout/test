//This requires can be configured (in future) to implement Dependency Injection if needed
var scrapper = require('../modules/scrapper');
var saver = require('../modules/save');

module.exports = {
  links: function LinksControllerLinks(req, res) {
      if (! req.body.url) return res.status(400).jsonp('Specify url');
      scrapper.getLinksFromUrl(req.body.url, function(result){
        saver.save(req.body.url, JSON.stringify(result), function() {
          res.jsonp(result)
        })
      })
    }
}
