Feature: Test all features
Scenario: Stage 1: run server
  Given simple node.js server
  Then it listens on http://localhost:9000
  Then http://localhost:9000/test response test for Iftach

Scenario: Stage 2: test endpoint
  Given POST endpoint: http://localhost:9000/api/links

Scenario: Stage 3.1: test example.com
  Given POST body { "url" : "http://example.com/" }
  Then JSON response { "links" : [ "http://www.iana.org/domains/example" ] }

Scenario: Stage 3.2: test frontend call
  Given open http://localhost:9000/testApiLinks.html
  Then testApiLinks contents equal { "links" : [ "http://www.iana.org/domains/example" ] }

Scenario: Stage 4: saving to file
  Then contents of ./links/example.com.json should be { "links" : [ "http://www.iana.org/domains/example" ] }

Scenario: Terminate server
  Then terminate server

Scenario: remove links files
  Then remove links files
