//exporting run function, so it can be used in index.js and in tests
var server;

module.exports = {
  run: function(cb) {
    // http module can be used, but piping outputs and other lowlevel stuff
    // break readability and single responsibility principle
    var express = require('express');
    var app = express();
    var bodyParser = require('body-parser');

    //serving public folder as static files
    app.use(express.static('public'));

    //setting default port and hostname
    var port = process.env.PORT || 9000;
    var hostname = process.env.HOSTNAME || '127.0.0.1';

    //Routes should be moved to routes file/files, but not if there are only two of them
    //route to test our server running on defined port
    //better to hide it on production, but ignored for simplicity
    app.get('/test', require('./lib/controllers/test').test);
    app.post('/api/links', bodyParser.json(), require('./lib/controllers/links').links);

    server = app.listen(port, hostname, function(){
      var host = server.address().address;
      var port = server.address().port;
      console.log('Test app listens on http://%s:%s', host, port);
      if (cb) cb();
    });
  },
  terminate: function(cb) {
    server.close(cb);
  }
}
